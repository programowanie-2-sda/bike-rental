package pl.example.michal;

import pl.example.michal.bikerental.BikeRentalApp;

public class MainApp {
    public static void main(String[] args) {
        BikeRentalApp bikeRentalApp = new BikeRentalApp();
        bikeRentalApp.runApp();
    }
}
