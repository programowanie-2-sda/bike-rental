package pl.example.michal.bike;

import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikeparameters.Color;

public class BicycleFactory extends BikeFactory {
    @Override
    public Bike createBike(Brand brand, Color color) {
        return new Bicycle(brand, color);
    }
}
