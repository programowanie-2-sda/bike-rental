package pl.example.michal.bike;

import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikeparameters.Color;
import pl.example.michal.bikeparameters.Status;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class Bike {
    private final Brand brand;
    private final Color color;
    private Status status;
    private LocalDateTime rentalTime;

    Bike(Brand brand, Color color) {
        this.brand = brand;
        this.color = color;
        status = Status.FREE;
    }

    public Brand getBrand() {
        return brand;
    }

    public Color getColor() {
        return color;
    }

    public Status getStatus() {
        return status;
    }

    public LocalDateTime getRentalTime() {
        return rentalTime;
    }

    public void setRentalTime(LocalDateTime rentalTime) {
        this.rentalTime = rentalTime;
    }

    public void setStatus(Status status) {
        this.status = status;
        if (status.equals(Status.RENTED)) {
            rentalTime = LocalDateTime.now();
        }
        if (status.equals(Status.FREE)) {
            rentalTime =null;
        }
    }

    @Override
    public String toString() {
        StringBuilder stringTime =  new StringBuilder(", rental time: ") ;
        if (status.equals(Status.RENTED)) {
            stringTime.append(rentalTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss")));
        }
        if(status.equals(Status.FREE)){
            stringTime.append(rentalTime);
        }
        return "Bike{" +
                "brand: " + brand +
                ", color: " + color +
                ", price/hour: $" + brand.getPriceForHour() +
                ", status: " + status +
                stringTime +
                '}';
    }
}
