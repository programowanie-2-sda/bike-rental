package pl.example.michal.bike;

import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikeparameters.Color;

public abstract class BikeFactory {
    public abstract Bike createBike(Brand brand, Color color);
}
