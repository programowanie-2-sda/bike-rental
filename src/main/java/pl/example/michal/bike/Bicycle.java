package pl.example.michal.bike;

import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikeparameters.Color;

class Bicycle extends Bike {
    Bicycle(Brand brand, Color color) {
        super(brand, color);
    }
}
