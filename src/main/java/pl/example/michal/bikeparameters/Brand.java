package pl.example.michal.bikeparameters;

public enum Brand {
    GIANT(15),
    MERIDA(12),
    ROMET(10);

    final double priceForHour;

    Brand(double priceForHour) {
        this.priceForHour = priceForHour;
    }

    public double getPriceForHour() {
        return priceForHour;
    }
}
