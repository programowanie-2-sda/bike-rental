package pl.example.michal.printer;

import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;
import pl.example.michal.user.User;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Printer {
    public static void printHeader(String text) {
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println(text);
        System.out.println("------------------------------------------------------------------------------------------");
    }

    public static void printWallet(double amount) {
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("YOUR WALLET: $" + Printer.decimalFormatter(amount));
        System.out.println("------------------------------------------------------------------------------------------");
    }

    public static void printUserBikes(User user) {
        user.getUserBikes().entrySet()
                .forEach(entry -> System.out.println(new StringBuilder().append(entry.getKey())
                        .append(" : ").append(entry.getValue())));
    }

    public static String formatDateTime(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");
        return localDateTime.format(formatter);
    }

    public static void printMapOfBikes(BikeDatabase bikeDatabase) {
        bikeDatabase.getMapOfBikes().entrySet().stream()
                .forEach(entry -> System.out.println(new StringBuilder().append(entry.getKey()).append(" : ")
                        .append(entry.getValue()).toString()));
    }

    public static String decimalFormatter(double amount) {
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(amount);
    }
}
