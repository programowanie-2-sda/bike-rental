package pl.example.michal.bikerental.menustyles;

import java.util.List;

public class StoreBikesMenu extends Menu {
    public StoreBikesMenu() {
        setMenuString("Press: 0-go back to Main Menu, 1-filter by color, 2-filter by brand, " +
                "\n3-filter by status, 4-filter by price per hour, 5-rent a bike");
        setListOfOptions(List.of(0, 1, 2, 3, 4, 5));
    }
}
