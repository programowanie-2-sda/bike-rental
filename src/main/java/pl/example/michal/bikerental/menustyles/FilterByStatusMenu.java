package pl.example.michal.bikerental.menustyles;

import java.util.List;

public class FilterByStatusMenu extends Menu {
    public FilterByStatusMenu() {
        setMenuString("Press: 0-go back to previous Menu, 1-filter FREE only, 2-filter RENTED only");
        setListOfOptions(List.of(0, 1, 2));
    }
}
