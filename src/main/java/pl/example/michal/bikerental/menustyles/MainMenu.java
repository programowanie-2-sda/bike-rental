package pl.example.michal.bikerental.menustyles;

import java.util.List;

public class MainMenu extends Menu {
    public MainMenu() {
        setMenuString("Press: 0-EXIT, 1-your wallet, 2-show all store's bikes, 3-show my bikes");
        setListOfOptions(List.of(0, 1, 2, 3));
    }
}
