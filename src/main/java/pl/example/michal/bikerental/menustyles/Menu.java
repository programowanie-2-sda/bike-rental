package pl.example.michal.bikerental.menustyles;

import pl.example.michal.bikerental.UserChoiceGetter;
import pl.example.michal.printer.Printer;

import java.util.List;

public abstract class Menu {
    private String menuString;
    private List<Integer> listOfOptions;

    public int getOption() {
        Printer.printHeader(menuString);
        return UserChoiceGetter.getOption(listOfOptions, menuString);
    }

    void setMenuString(String menuString) {
        this.menuString = menuString;
    }

    void setListOfOptions(List<Integer> listOfOptions) {
        this.listOfOptions = listOfOptions;
    }
}
