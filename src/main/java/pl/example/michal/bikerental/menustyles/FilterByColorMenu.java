package pl.example.michal.bikerental.menustyles;

import java.util.List;

public class FilterByColorMenu extends Menu {
    public FilterByColorMenu() {
        setMenuString("Press: 0-go back to previous Menu, 1-filter BLACK only, 2-filter RED only, " +
                "\n3-filter WHITE only");
        setListOfOptions(List.of(0, 1, 2, 3));
    }
}
