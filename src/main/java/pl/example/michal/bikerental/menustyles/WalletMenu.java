package pl.example.michal.bikerental.menustyles;

import java.util.List;

public class WalletMenu extends Menu {
    public WalletMenu() {
        setMenuString("Press: 0-go back to Main Menu, 1-add money to wallet");
        setListOfOptions(List.of(0, 1));
    }
}
