package pl.example.michal.bikerental.menustyles;

import java.util.List;

public class FilterByBrandMenu extends Menu {
    public FilterByBrandMenu() {
        setMenuString("Press: 0-go back to previous Menu, 1-filter GIANT only, 2-filter MERIDA only, " +
                "\n3-filter ROMET only");
        setListOfOptions(List.of(0, 1, 2, 3));
    }
}
