package pl.example.michal.bikerental.menustyles;

import java.util.List;

public class UserBikesMenu extends Menu {
    public UserBikesMenu() {
        setMenuString("Press: 0-go back to Main Menu, 1-return bike to store");
        setListOfOptions(List.of(0, 1));
    }
}
