package pl.example.michal.bikerental;

import pl.example.michal.printer.Printer;
import pl.example.michal.user.User;

class Start {
    private final User user;

    Start(User user) {
        this.user = user;
    }

    void startAplication() {
        String name = user.setUserName();
        Printer.printHeader("Hello, " + name + ". Welcome in BIKE RENTAL STORE !!!!");
    }
}
