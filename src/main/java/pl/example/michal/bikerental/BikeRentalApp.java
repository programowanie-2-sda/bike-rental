package pl.example.michal.bikerental;

import pl.example.michal.bikerental.mainmenu.Exit;
import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;
import pl.example.michal.bikerental.mainmenu.storebikes.StoreBikes;
import pl.example.michal.bikerental.mainmenu.userbikes.UserBikes;
import pl.example.michal.bikerental.mainmenu.Wallet;
import pl.example.michal.bikerental.menustyles.MainMenu;
import pl.example.michal.bikerental.menustyles.Menu;
import pl.example.michal.user.PrivateUser;
import pl.example.michal.user.User;

public class BikeRentalApp {
    private final BikeDatabase bikeDatabase;
    private final StoreBikes storeBikes;
    private final UserBikes userBikes;
    private final Start start;
    private final Exit exit;
    private final Menu menu;
    private final User user;
    private final Wallet wallet;

    public BikeRentalApp() {
        bikeDatabase = new BikeDatabase();
        user = new PrivateUser();
        storeBikes = new StoreBikes(bikeDatabase, user);
        start = new Start(user);
        exit = new Exit(user);
        menu = new MainMenu();
        wallet = new Wallet(user);
        userBikes = new UserBikes(bikeDatabase, user);
    }

    public void runApp() {
        start.startAplication();
        runMainMenu();
    }

    private void runMainMenu() {
        int input = menu.getOption();
        switch (input) {
            case 0: {
                exit.exitProgram();
                runMainMenu();
            }
            break;
            case 1: {
                input = wallet.show();
                if (input == 0) {
                    runMainMenu();
                }
            }
            break;
            case 2: {
                input = storeBikes.show();
                if (input == 0) {
                    runMainMenu();
                }
            }
            break;
            case 3: {
                input = userBikes.show();
                if (input == 0) {
                    runMainMenu();
                }
            }
        }
    }
}
