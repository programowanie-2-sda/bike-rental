package pl.example.michal.bikerental;

import pl.example.michal.printer.Printer;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class UserChoiceGetter {
    public static int getOption(List<Integer> options, String text) {
        int input = -1;
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        while (flag) {
            try {
                input = scanner.nextInt();
                flag = false;
                if (!options.contains(input)) {
                    flag = true;
                    System.out.println("Incorrect input. Please try once again.");
                    Printer.printHeader(text);
                }
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Incorrect input. Please try once again.");
                Printer.printHeader(text);
            }
        }
        return input;
    }

    public static double getPositiveAmount() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        double amount = 0;
        while (flag) {
            try {
                amount = scanner.nextDouble();
                flag = false;
                if (amount <= 0) {
                    Printer.printHeader("Amount has to be positive!!! Please try once again.");
                    flag = true;
                }
            } catch (InputMismatchException e) {
                Printer.printHeader("Cannot recognize amount. Please try again.");
                scanner.nextLine();
            }
        }
        return amount;
    }
}
