package pl.example.michal.bikerental.mainmenu.storebikes.filtering;

import pl.example.michal.bike.Bike;
import pl.example.michal.bikeparameters.Color;
import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;
import pl.example.michal.printer.Printer;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Predicate;

public class FilterByColor extends Filter<Color> {
    public FilterByColor(BikeDatabase bikeDatabase) {
        super(bikeDatabase);
    }

    @Override
    void printHeader(Color color) {
        Printer.printHeader("BIKES with color: " + color);
    }

    @Override
    Comparator<Map.Entry<Integer, Bike>> getComparator() {
        return Map.Entry.comparingByKey();
    }

    @Override
    Predicate<Map.Entry<Integer, Bike>> getEntryPredicate(Color color) {
        return entry -> entry.getValue().getColor().equals(color);
    }
}
