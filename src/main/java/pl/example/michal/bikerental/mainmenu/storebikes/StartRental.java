package pl.example.michal.bikerental.mainmenu.storebikes;

import pl.example.michal.bike.Bike;
import pl.example.michal.bikeparameters.Status;
import pl.example.michal.printer.Printer;
import pl.example.michal.user.User;

import java.util.Map;

class StartRental {
    private final BikeDatabase bikeDatabase;
    private final User user;

    StartRental(BikeDatabase bikeDatabase, User user) {
        this.bikeDatabase = bikeDatabase;
        this.user = user;
    }

    void rentBike(int bikeID) {
        if (bikeDatabase.getMapOfBikes().get(bikeID).getStatus().equals(Status.FREE)) {
            bikeDatabase.switchStatusForBike(bikeID);
            Printer.printHeader("Rental successful!");
            user.addBike(getBikeEntry(bikeID));
        } else {
            Printer.printHeader("Cannot rent this bike! Selected bike is already rented.");
        }
    }

    private Map.Entry<Integer, Bike> getBikeEntry(int bikeId) {
        return bikeDatabase.getMapOfBikes().entrySet().stream()
                .filter(entry -> entry.getKey() == bikeId)
                .findFirst()
                .get();
    }
}
