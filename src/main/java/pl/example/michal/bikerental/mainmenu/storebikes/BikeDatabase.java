package pl.example.michal.bikerental.mainmenu.storebikes;

import pl.example.michal.bike.BicycleFactory;
import pl.example.michal.bike.Bike;
import pl.example.michal.bike.BikeFactory;
import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikeparameters.Color;
import pl.example.michal.bikeparameters.Status;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BikeDatabase {
    private Map<Integer, Bike> mapOfBikes = new HashMap<>();

    public BikeDatabase() {
        createBikesDatabase();
    }

    public Map<Integer, Bike> getMapOfBikes() {
        return mapOfBikes;
    }

    private void createBikesDatabase() {
        BikeFactory bicycleFactory = new BicycleFactory();
        Bike bike1 = bicycleFactory.createBike(Brand.GIANT, Color.BLACK);
        Bike bike2 = bicycleFactory.createBike(Brand.GIANT, Color.RED);
        Bike bike3 = bicycleFactory.createBike(Brand.GIANT, Color.WHITE);
        Bike bike4 = bicycleFactory.createBike(Brand.GIANT, Color.BLACK);
        Bike bike5 = bicycleFactory.createBike(Brand.MERIDA, Color.BLACK);
        Bike bike6 = bicycleFactory.createBike(Brand.MERIDA, Color.RED);
        Bike bike7 = bicycleFactory.createBike(Brand.MERIDA, Color.WHITE);
        Bike bike8 = bicycleFactory.createBike(Brand.MERIDA, Color.WHITE);
        Bike bike9 = bicycleFactory.createBike(Brand.ROMET, Color.BLACK);
        Bike bike10 = bicycleFactory.createBike(Brand.ROMET, Color.RED);
        Bike bike11 = bicycleFactory.createBike(Brand.ROMET, Color.RED);
        Bike bike12 = bicycleFactory.createBike(Brand.ROMET, Color.BLACK);
        bike4.setStatus(Status.RENTED);
        bike4.setRentalTime(LocalDateTime.now().minusDays(5).minusHours(4));
        bike10.setStatus(Status.RENTED);
        bike4.setRentalTime(LocalDateTime.now().minusDays(1).minusHours(1).minusMinutes(15).minusSeconds(4));
        bike11.setStatus(Status.RENTED);
        List<Bike> list = List.of(bike1, bike2, bike3, bike4, bike5, bike6, bike7, bike8, bike9, bike10,
                bike11, bike12);
        for (int i = 0; i < list.size(); i++) {
            mapOfBikes.put(i + 1, list.get(i));
        }
    }

    public LocalDateTime getBikeStartRentalTime(Integer bikeId) {
        return mapOfBikes.get(bikeId).getRentalTime();
    }

    public void switchStatusForBike(int idOfBike) {
        Bike tempBike = mapOfBikes.get(idOfBike);
        Status status = tempBike.getStatus();
        if (status.equals(Status.FREE)) {
            tempBike.setStatus(Status.RENTED);
        } else {
            tempBike.setStatus(Status.FREE);
        }
        mapOfBikes.replace(idOfBike, tempBike);
    }
}
