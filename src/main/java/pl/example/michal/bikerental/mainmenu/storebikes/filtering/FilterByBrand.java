package pl.example.michal.bikerental.mainmenu.storebikes.filtering;

import pl.example.michal.bike.Bike;
import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;
import pl.example.michal.printer.Printer;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Predicate;

public class FilterByBrand extends Filter<Brand> {
    public FilterByBrand(BikeDatabase bikeDatabase) {
        super(bikeDatabase);
    }

    @Override
    void printHeader(Brand brand) {
        Printer.printHeader("BIKES with brand: " + brand);
    }

    @Override
    Comparator<Map.Entry<Integer, Bike>> getComparator() {
        return Map.Entry.comparingByKey();
    }

    @Override
    Predicate<Map.Entry<Integer, Bike>> getEntryPredicate(Brand brand) {
        return entry -> entry.getValue().getBrand().equals(brand);
    }
}
