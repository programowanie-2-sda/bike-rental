package pl.example.michal.bikerental.mainmenu.storebikes;

import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikeparameters.Color;
import pl.example.michal.bikeparameters.Status;
import pl.example.michal.bikerental.UserChoiceGetter;
import pl.example.michal.bikerental.mainmenu.storebikes.filtering.*;
import pl.example.michal.bikerental.menustyles.*;
import pl.example.michal.printer.Printer;
import pl.example.michal.user.User;

import java.util.ArrayList;
import java.util.List;

public class StoreBikes {
    private final BikeDatabase bikeDatabase;
    private final StartRental startRental;
    private Menu menu;
    private Filter filter;

    public StoreBikes(BikeDatabase bikeDatabase, User user) {
        this.bikeDatabase = bikeDatabase;
        menu = new StoreBikesMenu();
        startRental = new StartRental(bikeDatabase, user);
    }

    public int show() {
        showBikeDatabase();
        int input = 0;
        boolean flag = true;
        while (flag) {
            menu = new StoreBikesMenu();
            input = menu.getOption();
            switch (input) {
                case 0: {
                    flag = false;
                }
                break;
                case 1: {
                    filterByColor();
                }
                break;
                case 2: {
                    filterByBrand();
                }
                break;
                case 3: {
                    filterByStatus();
                }
                break;
                case 4: {
                    filterByPrice();
                }
                break;
                case 5: {
                    String text = "Please provide id of bike you would like to rent";
                    Printer.printHeader(text);
                    List<Integer> idsOfBikes = new ArrayList<>(bikeDatabase.getMapOfBikes().keySet());
                    int idOfBike = UserChoiceGetter.getOption(idsOfBikes, text);
                    startRental.rentBike(idOfBike);
                    showBikeDatabase();
                }
                break;
            }
        }
        return input;
    }

    private void filterByPrice() {
        filter = new FilterByPrice(bikeDatabase);
        Printer.printHeader("Please provide maximum price per hour");
        double max = UserChoiceGetter.getPositiveAmount();
        filter.filterBy(max);
    }

    private void filterByStatus() {
        filter = new FilterByStatus(bikeDatabase);
        int input;
        menu = new FilterByStatusMenu();
        input = menu.getOption();
        if (input == 1) {
            filter.filterBy(Status.FREE);
        } else if (input == 2) {
            filter.filterBy(Status.RENTED);
        } else {
            showBikeDatabase();
        }
    }

    private void filterByColor() {
        filter = new FilterByColor(bikeDatabase);
        int input;
        menu = new FilterByColorMenu();
        input = menu.getOption();
        if (input == 1) {
            filter.filterBy(Color.BLACK);
        } else if (input == 2) {
            filter.filterBy(Color.RED);
        } else if (input == 3) {
            filter.filterBy(Color.WHITE);
        } else {
            showBikeDatabase();
        }
    }

    private void filterByBrand() {
        filter = new FilterByBrand(bikeDatabase);
        int input;
        menu = new FilterByBrandMenu();
        input = menu.getOption();
        if (input == 1) {
            filter.filterBy(Brand.GIANT);
        } else if (input == 2) {
            filter.filterBy(Brand.MERIDA);
        } else if (input == 3) {
            filter.filterBy(Brand.ROMET);
        } else {
            showBikeDatabase();
        }
    }

    private void showBikeDatabase() {
        Printer.printHeader("ALL BIKES: ");
        Printer.printMapOfBikes(bikeDatabase);
    }
}
