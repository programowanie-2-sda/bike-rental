package pl.example.michal.bikerental.mainmenu.storebikes.filtering;

import pl.example.michal.bike.Bike;
import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Predicate;

public abstract class Filter<T> {
    private final BikeDatabase bikeDatabase;

    Filter(BikeDatabase bikeDatabase) {
        this.bikeDatabase = bikeDatabase;
    }

    public void filterBy(T item) {
        printHeader(item);
        bikeDatabase.getMapOfBikes().entrySet().stream()
                .filter(getEntryPredicate(item))
                .sorted(getComparator())
                .forEach(entry -> System.out.println(new StringBuilder().append(entry.getKey()).append(" : ")
                        .append(entry.getValue()).toString()));
    }

    abstract void printHeader(T item);

    abstract Comparator<Map.Entry<Integer, Bike>> getComparator();

    abstract Predicate<Map.Entry<Integer, Bike>> getEntryPredicate(T item);
}
