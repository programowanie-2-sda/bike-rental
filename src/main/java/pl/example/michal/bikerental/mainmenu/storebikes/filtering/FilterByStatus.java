package pl.example.michal.bikerental.mainmenu.storebikes.filtering;

import pl.example.michal.bike.Bike;
import pl.example.michal.bikeparameters.Status;
import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;
import pl.example.michal.printer.Printer;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Predicate;

public class FilterByStatus extends Filter<Status> {
    public FilterByStatus(BikeDatabase bikeDatabase) {
        super(bikeDatabase);
    }

    @Override
    void printHeader(Status status) {
        Printer.printHeader("BIKES with status: " + status);
    }

    @Override
    Comparator<Map.Entry<Integer, Bike>> getComparator() {
        return Map.Entry.comparingByKey();
    }

    @Override
    Predicate<Map.Entry<Integer, Bike>> getEntryPredicate(Status status) {
        return entry -> entry.getValue().getStatus().equals(status);
    }
}
