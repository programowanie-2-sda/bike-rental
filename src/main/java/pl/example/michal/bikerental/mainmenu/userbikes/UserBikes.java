package pl.example.michal.bikerental.mainmenu.userbikes;

import pl.example.michal.bikerental.UserChoiceGetter;
import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;
import pl.example.michal.bikerental.menustyles.Menu;
import pl.example.michal.bikerental.menustyles.UserBikesMenu;
import pl.example.michal.printer.Printer;
import pl.example.michal.user.User;

import java.util.List;

public class UserBikes {
    private final BikeDatabase bikeDatabase;
    private final User user;
    private final FinishRental finishRental;
    private Menu menu;

    public UserBikes(BikeDatabase bikeDatabase, User user) {
        this.bikeDatabase = bikeDatabase;
        this.user = user;
        finishRental = new FinishRental(bikeDatabase, user);
    }

    public int show() {
        showUserBikes();
        int input = 0;
        boolean flag = true;
        while (flag) {
            menu = new UserBikesMenu();
            input = menu.getOption();
            switch (input) {
                case 0: {
                    flag = false;
                }
                break;
                case 1: {
                    if(user.getUserBikes().isEmpty()){
                        System.out.println("Cannot perform this action. You don't have any bikes rented.");
                    } else {
                        String text = "Please provide id of bike you would like to return";
                        Printer.printHeader(text);
                        List<Integer> idsOfBikes = user.getListOfUserBikesIds();
                        int idOfBike = UserChoiceGetter.getOption(idsOfBikes, text);
                        finishRental.returnBike(idOfBike);
                        showUserBikes();
                    }
                }
                break;
            }
        }
        return input;
    }

    private void showUserBikes() {
        Printer.printHeader(user.getName() + " BIKES: ");
        if (user.getUserBikes().isEmpty()) {
            System.out.println("You don't have any rented bikes.");
        } else {
            Printer.printUserBikes(user);
        }
    }
}
