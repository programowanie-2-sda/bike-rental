package pl.example.michal.bikerental.mainmenu;

import pl.example.michal.printer.Printer;
import pl.example.michal.user.User;

public class Exit {
    private User user;

    public Exit(User user) {
        this.user = user;
    }

    public void exitProgram() {
        if (user.getWallet() >= 0) {
            Printer.printHeader("Thank you for your visit! Bye!!!");
            System.exit(1);
        } else {
            Printer.printHeader("Your wallet: $" + Printer.decimalFormatter(user.getWallet()) +
                    ". You need to pay your debt before exit!!!!");
        }
    }
}
