package pl.example.michal.bikerental.mainmenu;

import pl.example.michal.bikerental.UserChoiceGetter;
import pl.example.michal.bikerental.menustyles.Menu;
import pl.example.michal.bikerental.menustyles.WalletMenu;
import pl.example.michal.printer.Printer;
import pl.example.michal.user.User;

public class Wallet {
    private final User user;
    private final Menu menu;

    public Wallet(User user) {
        this.user = user;
        menu = new WalletMenu();
    }

    public int show() {
        Printer.printWallet(user.getWallet());
        boolean flag = true;
        int input = 0;
        while (flag) {
            input = menu.getOption();
            if (input == 1) {
                Printer.printHeader("Please provide amount.");
                double amount = UserChoiceGetter.getPositiveAmount();
                user.setWallet(user.getWallet() + amount);
                Printer.printWallet(user.getWallet());
            } else {
                flag = false;
            }
        }
        return input;
    }
}
