package pl.example.michal.bikerental.mainmenu.userbikes;

import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;
import pl.example.michal.printer.Printer;
import pl.example.michal.user.User;

import java.time.Duration;
import java.time.LocalDateTime;

class FinishRental {
    private final BikeDatabase bikeDatabase;
    private final User user;

    FinishRental(BikeDatabase bikeDatabase, User user) {
        this.bikeDatabase = bikeDatabase;
        this.user = user;
    }

    void returnBike(int bikeId) {
        double amountForRental = calculateRentalPrice(bikeId);
        bikeDatabase.switchStatusForBike(bikeId);
        user.setWallet(user.getWallet() - amountForRental);
        Printer.printHeader("Rental ended at: " + Printer.formatDateTime(LocalDateTime.now()) +
                ". Wallet charged for: $" + amountForRental);
        user.removeBike(bikeId);
    }

    private double calculateRentalPrice(int bikeId) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime bikeStartRentalTime = bikeDatabase.getBikeStartRentalTime(bikeId);
        double rentalDuration = Duration.between(bikeStartRentalTime, now).getSeconds();
        return rentalDuration / 10 * bikeDatabase.getMapOfBikes().get(bikeId)
                .getBrand().getPriceForHour();//supposing 1hour = 10 seconds
    }
}
