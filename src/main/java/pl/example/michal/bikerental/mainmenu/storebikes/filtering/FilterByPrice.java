package pl.example.michal.bikerental.mainmenu.storebikes.filtering;

import pl.example.michal.bike.Bike;
import pl.example.michal.bikerental.mainmenu.storebikes.BikeDatabase;
import pl.example.michal.printer.Printer;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Predicate;

public class FilterByPrice extends Filter<Double> {
    public FilterByPrice(BikeDatabase bikeDatabase) {
        super(bikeDatabase);
    }

    @Override
    void printHeader(Double price) {
        Printer.printHeader("BIKES with maximum price per hour: " + price);
    }

    @Override
    Comparator<Map.Entry<Integer, Bike>> getComparator() {
        return Map.Entry.comparingByValue(Comparator.comparing(entry -> entry.getBrand().getPriceForHour()));
    }

    @Override
    Predicate<Map.Entry<Integer, Bike>> getEntryPredicate(Double price) {
        return entry -> entry.getValue().getBrand().getPriceForHour() <= price;
    }
}

