package pl.example.michal.user;

import pl.example.michal.bike.Bike;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class User {
    private String name;
    private double wallet;
    private Map<Integer, Bike> userBikes = new LinkedHashMap<>();

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public double getWallet() {
        return wallet;
    }

    public void setWallet(double wallet) {
        this.wallet = wallet;
    }

    public Map<Integer, Bike> getUserBikes() {
        return userBikes;
    }

    public void addBike(Map.Entry<Integer, Bike> mapEntry) {
        userBikes.put(mapEntry.getKey(), mapEntry.getValue());
    }

    public void removeBike(Integer bikeId) {
        userBikes.remove(bikeId);
    }

    public List<Integer> getListOfUserBikesIds() {
        return new ArrayList<>(userBikes.keySet());
    }

    public abstract String setUserName();
}
