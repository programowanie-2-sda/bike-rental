package pl.example.michal.user;

import java.util.Scanner;

public class PrivateUser extends User {
    @Override
    public String setUserName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter user's name");
        String name = scanner.nextLine();
        setName(name);
        return name;
    }
}
