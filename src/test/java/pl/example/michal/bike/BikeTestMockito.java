package pl.example.michal.bike;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikeparameters.Color;
import pl.example.michal.bikeparameters.Status;

import static org.mockito.Mockito.mock;

class BikeTestMockito {

    Bike bike = mock(Bike.class);

    @Test
    void should_change_status_to_rented_when_current_status_is_free() {
        Mockito.when(bike.getStatus()).thenReturn(Status.FREE);
        bike.setStatus(Status.RENTED);
       Assertions.assertEquals(Status.RENTED, bike.getStatus());
    }

    @Test
    void should_change_rental_date_from_null() {
        bike.setStatus(Status.RENTED);
        Assertions.assertNotNull(bike.getRentalTime());
    }

    @Test
    void should_change_status_to_FREE_when_current_status_is_rented() {
        bike.setStatus(Status.RENTED);
        bike.setStatus(Status.FREE);
        Assertions.assertEquals(Status.FREE, bike.getStatus());
    }

    @Test
    void should_change_rental_date_from_current_to_null() {
        bike.setStatus(Status.RENTED);
        bike.setStatus(Status.FREE);
        Assertions.assertNull(bike.getRentalTime());
    }
}