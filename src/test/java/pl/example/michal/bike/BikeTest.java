package pl.example.michal.bike;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.example.michal.bikeparameters.Brand;
import pl.example.michal.bikeparameters.Color;
import pl.example.michal.bikeparameters.Status;

class BikeTest {
    private Bike bike;

    //czy tu moze jednak uzyc jakos mockito????
    @BeforeEach
    void setUp() {
        bike = new Bicycle(Brand.GIANT, Color.RED);
    }

    @Test
    void should_change_status_to_rented_when_current_status_is_free() {
       bike.setStatus(Status.RENTED);
       Assertions.assertEquals(Status.RENTED, bike.getStatus());
    }

    @Test
    void should_change_rental_date_from_null() {
        bike.setStatus(Status.RENTED);
        Assertions.assertNotNull(bike.getRentalTime());
    }

    @Test
    void should_change_status_to_FREE_when_current_status_is_rented() {
        bike.setStatus(Status.RENTED);
        bike.setStatus(Status.FREE);
        Assertions.assertEquals(Status.FREE, bike.getStatus());
    }

    @Test
    void should_change_rental_date_from_current_to_null() {
        bike.setStatus(Status.RENTED);
        bike.setStatus(Status.FREE);
        Assertions.assertNull(bike.getRentalTime());
    }
}