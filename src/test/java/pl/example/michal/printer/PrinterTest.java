package pl.example.michal.printer;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class PrinterTest {

    @ParameterizedTest(name = "Decimal formatter for double amount={1} should return result = {0}, so two digits " +
            "after comma with roudings ")
    @MethodSource("decimalFormatterData")
    void decimalFormatter_should_return_two_digits_after_comma_with_rounding(String result, double amount) {
        assertEquals(result, Printer.decimalFormatter(amount));
    }

    private static Stream<Arguments> decimalFormatterData() {
        return Stream.of(Arguments.of("7,23", 7.23353),
                Arguments.of("7,24", 7.23743),
                Arguments.of("7,20", 7.2),
                Arguments.of("7,23", 7.23399));
    }

    @Test
    @DisplayName("Should return date} in format dd-MM-yyyy, HH:mm:ss")
    void should_return_date_in_correct_format() {
        LocalDateTime localDateTime = LocalDateTime.of(2000, 01, 01, 10, 35, 15, 14);
        assertEquals("01-01-2000, 10:35:15", Printer.formatDateTime(localDateTime));
    }
}